import configparser
import gevent
import requests
from gevent.queue import Queue
from gevent.pywsgi import WSGIServer
from flask import Flask, render_template, redirect, request, session, Response, url_for, current_app
from ServerSentEvent import ServerSentEvent


class SSEPush:

	def __init__(self):
		self.config = configparser.ConfigParser()
		self.config.read('config.ini')
		self.app = Flask(__name__)
		self.server = None
		self.subscriptions = []

	# Abonnieren: Endpoint, um mit Javascript auf Event-Queue zugreifen zu können
	def subscribe(self):
		def gen():
			q = Queue()
			self.subscriptions.append(q)
			try:
				while True:
					result = q.get()
					ev = ServerSentEvent(str(result))
					yield ev.encode()
			except GeneratorExit:  # Or maybe use flask signals
				self.subscriptions.remove(q)
		return Response(gen(), mimetype="text/event-stream")

	# Diese Funktion braucht dict als Input, baut daraus eine Post-Request an req_publish()
	def publish(self, msg):
		request_url = 'http://%s:%d/req_publish/%s' % (self.config['master']['flask_url'], int(self.config['DEFAULT']['flask_port']), msg)
		response = requests.get(request_url)
		print("published %s, received %s" % (msg, response.text))

	# Baut den von publish() erhaltenen dict in einen String um und sendet den als Queue an self.subscriptions
	def req_publish(self, msg):
		def notify(msg):
			for sub in self.subscriptions[:]:
				sub.put(msg)
		gevent.spawn(notify, msg=msg)
		return "OK"

	def add_all_endpoints(self):
		# SSE
		self.app.add_url_rule('/subscribe', 'subscribe', self.subscribe, methods=['GET', 'POST'])
		self.app.add_url_rule('/req_publish/<msg>', 'req_publish', self.req_publish, methods=['GET'])

	def run(self):
		self.add_all_endpoints()
		self.server = WSGIServer(self.config['flask_port'], self.app)
		self.server.serve_forever()
		print("something")
		